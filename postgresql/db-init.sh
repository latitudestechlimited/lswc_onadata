#!/bin/bash
set -e

gosu postgres psql -c --username "postgres" <<-EOSQL
    CREATE USER onadata WITH PASSWORD 'onadata';
    CREATE DATABASE onadata OWNER onadata;
EOSQL
